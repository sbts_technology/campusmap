@extends('layouts.theme')

@section('content')

<div id="imageSection" v-on:click="logParkingSpot">
    <canvas id="myCanvas" width="200" height="100" style="position: absolute; z-index: 5; border: 2px solid green"></canvas>
    <img id="image" src="images/Lot2S_1cropped.png" style="opacity: 1.0; position: relative">

    <!-- <div v-if="displayCursorPos">
        <p>@{{ cursorPosX }}</p>
    </div>  -->
    <p>@{{ test }}</p>
</div>

<script>
    const vm = new Vue({
        data: {
            test: 'Cool',
            canvas: '',
            image: '',
            scale: 0,
            displayCursorPos: false, 
            parkingSpots: [[769, 431, 877, 471, 1247, 425, 1118, 390],
                           [1184, 258, 1273, 272, 1445, 260, 1371, 243],
                           [1372, 288, 1482, 305, 1663, 289, 1570, 273],
                           [1258, 166, 1321, 175, 1342, 155, 1283, 149]
                          ],
            testSpot: [],
        },
        computed: {
            cursorPosX: function () {
                return 
            },
        },
        methods: {
            initializeCanvas: function () {
                this.canvas = document.getElementById("myCanvas");
                this.image = document.getElementById("image");
                // console.log(this.canvas);
                // console.log(this.image);
                this.canvas.width = image.clientWidth;
                this.canvas.height = image.clientHeight;
                // console.log(this.canvas);
                console.log(this.canvas.width);
                console.log(this.canvas.height);
            },
            getDistanceBetween2Pts: function (x1, y1, x2, y2) {
                var a = x2 - x1;
                var b = y2 - y1;
                return Math.sqrt(a*a + b*b);
            },
            getMaxSideLengthOfRhombus: function (pts) {
                return Math.max(this.getDistanceBetween2Pts(pts[0], pts[1], pts[2], pts[3]), this.getDistanceBetween2Pts(pts[2], pts[3], pts[4], pts[5]));
            },
            getMinSideLengthOfRhombus: function (pts) {
                return Math.min(this.getDistanceBetween2Pts(pts[0], pts[1], pts[2], pts[3]), this.getDistanceBetween2Pts(pts[2], pts[3], pts[4], pts[5]));
            },
            getAvrSideLengthOfRhombus: function (pts) {
                return (this.getDistanceBetween2Pts(pts[0], pts[1], pts[2], pts[3]) + this.getDistanceBetween2Pts(pts[2], pts[3], pts[4], pts[5]))/2;
            },
            drawParkingSpot: function (basepts, color, borderAlpha, regionAlpha) {
                var p = basepts;

                // set pixel height for boxes
                // var h = 0.35*this.getMaxSideLengthOfRhombus(p);
                // var h = this.getMinSideLengthOfRhombus(p);
                // var h = this.getAvrSideLengthOfRhombus(p);
                var h = Math.min(0.8*this.getAvrSideLengthOfRhombus(p), 100);
                // var h = 100;
                var yCoords = [p[1], p[3], p[5], p[7]];
                // find point with minimum y (furthest point from the vantage of the camera)
                var excludedIndex = (yCoords.indexOf(Math.min(...yCoords)) * 2) + 1;

                // this.drawQuadrilateral(p, color, borderAlpha, regionAlpha); // bottom
                this.drawQuadrilateral([p[0], p[1]-h, p[2], p[3]-h, p[4], p[5]-h, p[6], p[7]-h], color, borderAlpha, regionAlpha); // top

                // draw two of four sides of box
                if (![1, 3].includes(excludedIndex)) {
                    this.drawQuadrilateral([p[0], p[1], p[2], p[3], p[2], p[3]-h, p[0], p[1]-h], color, borderAlpha, regionAlpha);
                }
                if (![3, 5].includes(excludedIndex)) {
                    this.drawQuadrilateral([p[2], p[3], p[4], p[5], p[4], p[5]-h, p[2], p[3]-h], color, borderAlpha, regionAlpha);
                }
                if (![5, 7].includes(excludedIndex)) {
                    this.drawQuadrilateral([p[4], p[5], p[6], p[7], p[6], p[7]-h, p[4], p[5]-h], color, borderAlpha, regionAlpha);
                }
                if (![7, 1].includes(excludedIndex)) {
                    this.drawQuadrilateral([p[6], p[7], p[0], p[1], p[0], p[1]-h, p[6], p[7]-h], color, borderAlpha, regionAlpha);
                }
            },
            drawQuadrilateral: function (pts, color, borderAlpha, regionAlpha) {
                this.drawRegion(pts, color, borderAlpha, regionAlpha);
            },
            drawRectangle: function () {
                this.drawRegion([10, 10, 10, 100, 100, 100, 100, 10], '#0000FF', 1.0, 0.08);
            },
            drawRegion: function (pts , color, borderAlpha, regionAlpha) {
                //Note: you must call self.initializeCanvas() prior to calling this function for the first time
                var ctx = this.canvas.getContext("2d");
                ctx.globalAlpha = borderAlpha; 
                ctx.fillStyle = color;
                ctx.strokeStyle = color;
                
                ctx.beginPath();
                ctx.moveTo(pts[0], pts[1]);
                for (var j = 2; j < pts.length; j+=2) {
                    ctx.lineTo(pts[j], pts[j+1]);
                }
                ctx.closePath();
                ctx.stroke();
                ctx.globalAlpha = regionAlpha;
                ctx.fill(); // draw the region
            },
            displayCursorPosition: function (event) {
                this.displayCursorPos = true;
                var mouseCoords = this.getRelativeMouseCoords(event, this.image);
                console.log(mouseCoords);
            },
            logParkingSpot: function (event) {
                var mouseCoords = this.getRelativeMouseCoords(event, this.image);
                this.testSpot.push(mouseCoords.x, mouseCoords.y);
                console.log(this.testSpot.join(', '));
            },
            getRelativeMouseCoords: function (event, element) {
                var bounds = element.getBoundingClientRect();
                var mouseX = event.clientX - bounds.left;
                var mouseY = event.clientY - bounds.top;
                
                return {x: mouseX, y: mouseY};
            }
        },
        mounted: function () {
            this.initializeCanvas();
            // this.drawRectangle();
            // this.drawQuadrilateral([769, 431, 877, 471, 1247, 425, 1118, 386], '#0000FF', 1.0, 0.08);
            // this.drawQuadrilateral([769, 431-100, 877, 471-100, 1247, 425-100, 1118, 386-100], '#0000FF', 1.0, 0.08);
            this.parkingSpots.forEach(element => {
                this.drawParkingSpot(element, '#0000FF', 1.0, 0.08);
            });
            // this.drawParkingSpot([769, 431, 877, 471, 1247, 425, 1118, 390], '#0000FF', 1.0, 0.08);
        }

    }).$mount('#imageSection');
</script>

@endsection

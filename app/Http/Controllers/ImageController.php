<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function display() : View
    {
        return view('image');
    }
}
